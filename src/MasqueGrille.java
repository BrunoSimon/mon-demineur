import java.util.Scanner;

public class MasqueGrille {

	// attributs

	private int[][] MaskField;
	private int[][] copieMineField;
	private int Nrow;
	private int Nbomb;
	private int nCaseMasked;
	private int lineClick;
	private int columnClick;

	static final private int notClicked = -1;

	// methodes

	public MasqueGrille(GrilleBombe grille) {
		// constructeur
		Nrow = grille.getNrow();
		Nbomb = grille.getNbomb();
		nCaseMasked = Nrow * Nrow;
		copieMineField = grille.getGrille(); // meme element?
		InitMask();
	}

	private void InitMask() {
		MaskField = new int[Nrow][Nrow]; // nouveau tableau

		for (int i = 0; i < Nrow; i++)
			for (int j = 0; j < Nrow; j++)
				MaskField[i][j] = notClicked;
	}

	public String toString() {
		String strMask = "Grille de jeux:\n";
		for (int i = 0; i < Nrow; i++) {
			strMask += "   ";
			for (int j = 0; j < Nrow; j++) {
				if (MaskField[i][j] == notClicked)
					strMask += " �";
				else
					strMask += " " + MaskField[i][j];
			}
			strMask += "\n";
		}
		strMask += "Nombre de cases masqu�es=" + nCaseMasked + ", nbomb=" + Nbomb;
		return strMask;
	}

	public boolean testBomb() {
		/*
		 * test si une case est une bombe, sinon demasque la case. si la case n'est pas
		 * voisine d'autre bombes alors on demasque les cases voisines tant que des
		 * cases n'ont pas de bombes voisines.
		 */
		clickWhat();
		System.out.println("Click sur: (" + columnClick + ";" + lineClick + ")");

		if (copieMineField[lineClick][columnClick] == GrilleBombe.bombVal) {
			// System.out.println("GAME OVER!!");
			return false;
		} else {
			if (MaskField[lineClick][columnClick] == notClicked) {
				if (copieMineField[lineClick][columnClick] == 0) {
					flood(lineClick, columnClick);
				} else {
					UnMask(lineClick, columnClick);
				}
			}
			return true;
		}
	}

	private void clickWhat() {
		// choix par l'utilisateur de la case � tester.
		Scanner sc = new Scanner(System.in);
		do {
			System.out.println("choix column dans (0;" + (Nrow - 1) + "):");
			columnClick = sc.nextInt();

			if (0 <= columnClick && columnClick < Nrow) {
				System.out.println("choix line dans (0;" + (Nrow - 1) + "):");
				lineClick = sc.nextInt();
			} else {
				System.out.println("Pas valide!!");
			}
			if (!(0 <= lineClick && lineClick < Nrow))
				System.out.println("Pas Valide!!");
		} while (!(0 <= lineClick && lineClick < Nrow && 0 <= columnClick && columnClick < Nrow));
	}

	public boolean CanKeepPlaying() {
		// vrai tant qu'il y a plus de cases masqu�es que de bombes
		return nCaseMasked > Nbomb;
	}

	private void flood(int i, int j) {
		// demasquage par methode de remplissage 
		if (MaskField[i][j] == notClicked && copieMineField[i][j] == 0) {
			// demasque les cases voisines:
			for (int line = Math.max(i - 1, 0); line < Math.min(Nrow, i + 2); line++)
				for (int col = Math.max(j - 1, 0); col < Math.min(Nrow, j + 2); col++) {
					if ((i != line || j != col  && MaskField[line][col] == notClicked)) {
						flood(line, col);
					}
					UnMask(line, col);
				}
		}
	}

	private void UnMask(int i, int j) {
		// demasquage et modification du nombre de cases encore masqu�es
		if (MaskField[i][j] == notClicked) {
			nCaseMasked--;
			MaskField[i][j] = copieMineField[i][j];
		}
	}
}
