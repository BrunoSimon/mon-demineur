import java.util.Scanner;

public class ParametreJeu {

	// attributs
	private int Nrow; // nombre de ligne et colonne de la grille: user defined
	private int Nbomb; // nombre de bombes: user defined
	private int RowMax; // nombre max de row
	private int RowMin; // nombre min
	private int BombMin;
	private int BombMax;

	// methodes
	public ParametreJeu(int row, int bomb) {
		// constructeur sans choix de l'utilisateur
		RowMax = 20;
		RowMin = 5;
		BombMin = 5;
		Nrow = row;
		BombMax = Nrow * Nrow / 2;
		Nbomb = bomb;
	}

	public ParametreJeu() {
		// constructeur avec choix du nb de rows et bombes par l'utilisateurs
		RowMax = 20;
		RowMin = 5;
		BombMin = 5;
		SelectNrow();
		BombMax = Nrow * Nrow / 2;
		SelectNBomb();
	}

	private void SelectNrow() {
		// Selection du nombre de colonne par le joueur
		// entre RowMin et RowMax
		Scanner sc = new Scanner(System.in);

		System.out.println("Choisir la taille de grille:");
		do {
			System.out.println("Le choix se fait entre: " + RowMin + " et " + RowMax);
			Nrow = sc.nextInt();
		} while (Nrow < RowMin || Nrow > RowMax);
		System.out.println("Ok!");
	}

	private void SelectNBomb() {
		// Selection du nombre de bombes par le joueur
		// entre BombMin et BombMax
		Scanner sc = new Scanner(System.in);

		System.out.println("Choisir le nombre de bombes:");
		do {
			System.out.println("Choix entre " + BombMin + " et " + BombMax);
			Nbomb = sc.nextInt();
		} while (Nbomb < BombMin || Nbomb > BombMax);
		System.out.println("Ok!");
	}

	public void ShowConf() {
		// rappel de la configuration du denmineur
		System.out.println("La grille fait : " + Nrow + "x" + Nrow);
		System.out.println("Il y a " + Nbomb + " bombes � trouver.");
	}

	public int getNrow() {
		return Nrow;
	}

	public int getNbomb() {
		return Nbomb;
	}

}
