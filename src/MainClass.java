
public class MainClass {

	public static void main(String[] args) {
		// TODO : Timer
		// TODO : graphique

		System.out.println("Welcome to demineur: Console Boot Edition!");

		// lecture de la configuration de jeu
		ParametreJeu param = new ParametreJeu(10,10);
		param.ShowConf();

		// creation de la grille avec les mines
		GrilleBombe grille = new GrilleBombe(param);
		System.out.println(grille);

		// application du masque sur la grille
		MasqueGrille mask = new MasqueGrille(grille);
		System.out.println("Masque de la grille: ");
		System.out.println(mask);

		// jeux
		boolean test = true;
		boolean keep = true;
		do {
			test = mask.testBomb();
			keep = mask.CanKeepPlaying();
			// System.out.println(grille);
			System.out.println(mask);
		} while (test && keep);

		System.out.println("\n" + grille);
		if (test == true)
			System.out.println("C'EST GAGNE!!");
		else 
			System.out.println("GAME OVER!!");

		System.out.println("Thanks for playing!");
	}
}
