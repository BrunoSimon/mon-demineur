
public class GrilleBombe {

	// attributs
	private int Nrow;
	private int Nbomb;
	private int[][] Minefield;

	static public final int bombVal = 9;

	// methodes:
	public GrilleBombe(ParametreJeu param) {
		// constructeur
		Nrow = param.getNrow();
		Nbomb = param.getNbomb();
		Minefield = new int[Nrow][Nrow];
		ajoutBombs();
		rechercheVoisins();
	}

	public int[][] getGrille() {
		return Minefield;
	}

	public int getNrow() {
		return Nrow;
	}

	public int getNbomb() {
		return Nbomb;
	}

	private void ajoutBombs() {
		// ajout des bombes sur des positions pseudo-aleatoires
		int ibomb, jbomb;

		for (int i = 0; i < Nbomb; i++) {
			do {
				ibomb = (int) (Math.random() * (double) Nrow);
				jbomb = (int) (Math.random() * (double) Nrow);
			} while (Minefield[ibomb][jbomb] == bombVal); // on cherche une position tant que la bombe n'est pas plac�
															// sur une case vide

			Minefield[ibomb][jbomb] = bombVal;
		}
	}

	public String toString() {
		String strGrille = "Grille de bombes (X):\n";

		for (int i = 0; i < Nrow; i++) {
			strGrille += "   ";
			for (int j = 0; j < Nrow; j++) {
				if (Minefield[i][j] == bombVal)
					strGrille += " X";
				else
					strGrille += " " + Minefield[i][j];
			}
			strGrille += "\n";
		}
		return strGrille;
	}

	private void rechercheVoisins() {
		// recherche pour chaques cases le nombre de bombes voisines
		for (int index = 0; index < Nrow; index++)
			for (int jndex = 0; jndex < Nrow; jndex++)
				if (Minefield[index][jndex] != bombVal)
					Minefield[index][jndex] = nombreDeBombesVoisins(index, jndex);
	}

	private int nombreDeBombesVoisins(int col, int line) {
		// calcul le nombre de bombes adjacentes � une case line,col
		int total = 0;
		for (int i = Math.max(col - 1, 0); i < Math.min(Nrow, col + 2); i++)
			for (int j = Math.max(line - 1, 0); j < Math.min(Nrow, line + 2); j++)
				if ((i != col || j != line) && (Minefield[i][j] == bombVal))
					total++;
		return total;
	}

}
